<?php

use App\Customs\AES128;
use App\Models\Location;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }

    public function testFunc1() {
        $testFinds = [
            '(())((()))()()(()',
            '()(())((()))',
            '(((())))',
            '((()))(',
            '((()))))',
            '(((()())'
        ];
        $result = [];
        foreach ($testFinds as $testFind) {
            $result[] = $this->testFind($testFind);
        }
        var_dump($result);
    }

    /*
     * function
     * ada string () *valid if (()) ()
     * @return boolean
     * //list test :
     * - (())((()))()()(() | false
     * - ()(())((())) | true
     * - (((()))) | true
     * - ((()))( | false
     * - ((())))) | false
     * - (((()()) | false
     */
    public function testFind($myChar) {
        if($myChar[0] == ')') return false;

        $countChar = strlen($myChar);
        $counter = 0;

        for ($i = 0; $i < $countChar; $i++) {
         if($myChar[$i] == '(') $counter++;
         else $counter--;

         if($counter < 0) return false;
        }

        if($counter != 0) return false;
        return true;
    }

    //
    public function testFinds($myChar) {
        $openChar = ['(', '{', '<', '['];
        $closeChar = [')', '}', '>', ']'];

        if(in_array($myChar[0], $closeChar)) return false;

        $countChar = strlen($myChar);
        $counter = [0, 0, 0, 0];

        for ($i = 0; $i < $countChar; $i++) {
            if($searchOpen = array_search($myChar[$i], $openChar)) $counter[$searchOpen]++;
            elseif($searchClose = array_search($myChar[$i], $closeChar)) $counter[$searchClose]--;

            foreach ($counter as $item) {
                if($item < 0) return false;
            }
        }

        foreach ($counter as $item) {
            if($item != 0) return false;
        }

        return true;
    }

    public function testGetReport() {
        $reports = Location::getReports('2019-07');
        var_dump($reports);
        $this->assertTrue(true);
    }

    public function testDate() {
        $carbon = \Illuminate\Support\Carbon::now()->format('Y-m');
        dump($carbon);
        $this->assertTrue(true);
    }

    public function testAESdec() {
        $key = 'eyJpdiI6IkZ6ZGo5am5QNmFVZnJFblpVVGxRYUE9PSIsInZhbHVlIjoiUFYzN3lPK25za0RKZWdiMjE3WTJaUUNrR1hkTEs5TTNTWG9vNzNmRHJvVGtJdXIwUWUxWEQ0TndaTVB2VzBcL0MiLCJtYWMiOiJmMjJhNTlmNjQxZGI1MGQ4NzVjN2FlM2IwNTdhYWU5MzJmZmU2NGJjZWRjY2RhYjk0MmNkNzg2MzgzNmI3Zjc4In0=';
        $enc = 'i0vgQPmgAKdV3ASXYOVj1CJKWIrsoCVHaa85XAaJu5nF970gHvk0tGLfkYLq+iaYUbVGZ+9eu1+RMs76Og9YaFEy2Bdp105Wdq3MLEpsQLPyMm1YV1dvP7yq9GXb7I7mx8Hqz6wAmE0bQOywrX75/v1wJU1WeXK20QjL5bHGOG1u6KVmaSJBZ7qMEMlhL5Uv2DHuK5g7viAtfH1yK79slweEm5YLWoZ3oNePL27G8oQzMldJetCqqXehqJ/sKbs2F0JFokVFR6xt/FgMu3R/FJrfTqSgujT+A6hceqIOMTOXg5GfE4LJVH5bVZgpTqaCHHhnb8MsGVRQkSO5H2OkIeiThu01h3P+0nrEGzKs3G8R6S4E3Np6SWiqpXfKroZRkxxrZMZ2HuVtEIzqeOkouHEINN16IrSK9Lh3u8JLjDwm0l8ftDtivIMIItGD2af+E+SKGWWfDZOG4J3chtXSj2jaE4x3OFAcL9DTkmqt9EhXAoWO29j1sLkAQVPe9W/k9pqRGdoT7aNJMEqQkqq+fDX95s4EpZ1suTqDtGbHOBexd1n7sawefeQuPbMFB3sXQo0c9Gv9Ez8+DgEYTOWGdUPQ0wPZWk4QcD0pvxB1EmtTn0sy1W7ldEMe/1askWmvVYPWsE/4Vl1qrd6tT2V/t3hP+RIKZ040LOGPsGjxIlyxUPNrxPQ+OPBYW5KCDjkVQfGtZdHnarQb4SLa9NNzf+0tSRn+XCvzZGAMRs2B0cFAu94nyTR0xYtIoQXElMYj0tgsfrfPqYcLNYUMGeRDWicowOi9WWVASowhEaMzloAscNG1ZA3wKTFGH2mBWKVqaY/uE+B/Ccfg3H3FujW/AA==';

        $aesDec = AES128::decrypt($enc, $key);
        echo  "\n";
        dump($aesDec);
        $this->assertTrue(true);
    }

    public function testAESenc() {
        $key = '1234567890123456';
        $data = 'Nurma fadilah';
        $enc = AES128::encrypt($data, $key);
        dump($enc);

        $this->assertTrue(true);
    }
}
