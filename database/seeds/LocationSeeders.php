<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = DB::table('locations')->get();
        if($locations->isEmpty())
        DB::table('locations')->insert([
            ['id' => 1, 'location' => 'DKI Jakarta'],
            ['id' => 2, 'location' => 'Jawa Barat'],
            ['id' => 3, 'location' => 'Jawa Tengah'],
            ['id' => 4, 'location' => 'Jawa Timur'],
            ['id' => 5, 'location' => 'SUMATERA']
        ]);
    }
}
