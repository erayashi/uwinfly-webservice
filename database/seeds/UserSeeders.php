<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class UserSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = DB::table('users')->get();
        if($users->isEmpty())
        DB::table('users')->insert([
            [
                'location_id'   => '1',
                'full_name'     => 'admin',
                'username'      => 'admin',
                'email'         => 'admin@uwinfly.id',
                'address'       => 'Jl. the truth',
                'postal_code'   => '13140',
                'phone_number'  => '0811111123132',
                'password'      => Crypt::encrypt('admin123'),
                'is_admin'      => true
            ],
            [
                'location_id'   => '2',
                'full_name'     => 'Nurma',
                'username'      => 'nurmafadila',
                'email'         => 'nurma@uwinfly.id',
                'address'       => 'Jl. jakarta',
                'postal_code'   => '11110',
                'phone_number'  => '0811111123135',
                'password'      => Crypt::encrypt('nurma123'),
                'is_admin'      => true
            ]
        ]);
    }
}
