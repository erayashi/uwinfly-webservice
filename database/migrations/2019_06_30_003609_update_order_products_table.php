<?php

use App\Customs\MySchema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrderProductsTable extends Migration
{
    private $productOrder;

    public function __construct()
    {
        $this->productOrder = MySchema::init('order_products');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->productOrder->modifiedWhenColumn(['product_type', 'product_color'], function (Blueprint $table) {
            $table->string('product_type');
            $table->string('product_color');
        }, false);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->productOrder->dropColumnIfExist(['product_type', 'product_color']);
    }
}
