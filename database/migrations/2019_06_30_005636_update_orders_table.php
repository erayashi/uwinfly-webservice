<?php

use App\Customs\MySchema;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersTable extends Migration
{
    private $order;

    public function __construct()
    {
        $this->order = MySchema::init('orders');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->order->modifiedWhenColumn(['sales_name', 'customer_name'], function (Blueprint $table) {
            $table->string('sales_name');
            $table->string('customer_name');
        }, false);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->order->dropColumnIfExist(['sales_name', 'customer_name']);
    }
}
