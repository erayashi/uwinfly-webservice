<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var \Laravel\Lumen\Routing\Router $router */

$router->get('/', function () {
    echo '<html>
<head><meta name="google-site-verification" content="sw1rkBHHbl9Z2UBGuPsZokGlTyQh5twctpnYv1HsM3s" /></head>
wehehehe
</html>';
});

$router->group(['prefix' => 'api/v1'], function () use ($router) {
    $router->post('/auth', 'UserController@login');

    $router->group(['middleware' => 'auth'], function () use ($router) {

        $router->group(['middleware' => 'admin'], function () use ($router) {
            $router->post('/user', 'UserController@create');
            $router->patch('/user/{userID}', 'UserController@update');

            $router->post('/sales', 'SalesController@create');
            $router->patch('/sales/{salesID}', 'SalesController@update');
            $router->delete('/sales/{salesID}', 'SalesController@disable');

            $router->post('/product', 'ProductController@create');
            $router->patch('/product/{productID}', 'ProductController@update');
            $router->delete('/product/{productID}', 'ProductController@delete');
            $router->patch('/product/{productID}/stock', 'ProductController@updateStock');

            $router->get('/report', 'LocationController@getReport');
        });

        $router->get('/user', 'UserController@get');

        $router->get('/sales', 'SalesController@gets');
        $router->get('/sales/{salesID}', 'SalesController@get');

        $router->post('/customer', 'CustomerController@add');
        $router->get('/customers', 'CustomerController@gets');
        $router->get('/customer/{customerID}', 'CustomerController@get');

        $router->get('/products', 'ProductController@gets');
        $router->get('/product/{productID}', 'ProductController@get');

        $router->post('/order', 'OrderController@create');
        $router->delete('/order/{orderID}', 'OrderController@delete');
        $router->get('/orders', 'OrderController@gets');
        $router->get('/order/{orderID}', 'OrderController@get');
    });
});
