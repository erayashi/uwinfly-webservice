<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 4/21/2019
 * Time: 17:57
 */

$app = require __DIR__.'/../repositories/uwinfly-webservice/bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$app->run();
