#!/bin/sh

date
echo "script start"

PHP_CLI=/usr/local/bin/php
COMPOSER="$HOME/bin/composer.phar"
PROJECT=$1

if [[ ! -f "$COMPOSER" ]]
then
    echo "instaling composer"

    mkdir ~/bin
    cd ~/bin
    ${PHP_CLI} -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    ${PHP_CLI} -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    ${PHP_CLI} composer-setup.php
    ${PHP_CLI} -r "unlink('composer-setup.php');"

    echo "composer installer successfully"
else
    echo "composer already installed"
fi

${PHP_CLI} ${COMPOSER} --version

echo "check project $PROJECT/bootstrap/app.php"
if [[ (-f "$PROJECT/bootstrap/app.php") && (-f ${COMPOSER}) ]]
then
    echo "build your project"

    cd ${PROJECT}

    ${PHP_CLI} ${COMPOSER} update --no-scripts
    chmod +x ./artisan

    ${PHP_CLI} artisan migrate
    ${PHP_CLI} artisan db:seed

    echo "project build successfully"
else
    echo "project not found"
fi

date
echo "script end"
