<?php
/**
 * Created by eaz.
 * Date: 10/03/19
 * Time: 20.18
 * Github: https://github.com/erry-az
 */

namespace App\Http\Middleware;


use App\Customs\Response;
use App\Http\Errors\AuthError;
use App\Models\Token;
use App\Models\User;
use Closure;
use Exception;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('token');

        if(empty($token))
            return Response::Error(AuthError::class, 'HEADERS_NOT_COMPLETE',
                Response::HTTP_UNAUTHORIZED);

        try {
            $token = Token::check($token);
            $user = User::query()->find($token->id);

            $request->token = $user;
            if(is_null($user)) throw new Exception('Token is invalid');
            if($token->exp < time()) throw new Exception('Token is invalid');
        } catch (Exception $e) {
            return Response::Error(AuthError::class, 'CHECK_FAILED',
                Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }
}
