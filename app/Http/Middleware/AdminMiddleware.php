<?php
/**
 * Created by eaz.
 * Date: 31/03/19
 * Time: 16.50
 * Github: https://github.com/erry-az
 */

namespace App\Http\Middleware;


use App\Customs\Response;
use App\Http\Errors\AuthError;
use App\Models\User;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset($request->token))
            return Response::Error(AuthError::class, 'CHECK_FAILED',
                Response::HTTP_UNAUTHORIZED);

        /** @var User $token */
        $token = $request->token;
        if(!$token->is_admin)
            return Response::Error(AuthError::class, 'ACCESS_INVALID',
                Response::HTTP_FORBIDDEN);

        return $next($request);
    }
}
