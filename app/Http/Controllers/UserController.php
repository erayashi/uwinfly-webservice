<?php
/**
 * Created by eaz.
 * Date: 10/03/19
 * Time: 11.40
 * Github: https://github.com/erry-az
 */

namespace App\Http\Controllers;


use App\Customs\Response;
use App\Http\Errors\AuthError;
use App\Http\Errors\UserError;
use App\Models\Token;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;

class UserController extends AController
{
    public function get() {
        return $this->encSuccess($this->getToken());
    }

    public function login() {
        $auth = $this->getBasic(true);

        if($auth) {
            $username = $auth[0];
            $password = $auth[1];
            /** @var User $user */
            $user = User::query()
                ->where('username', $username)
                ->orWhere('email', $username)->first();

            if(!empty($user)) {
                $expiredAt = Carbon::now()->addMonth()->timestamp;

                try {
                    $decPassword = Crypt::decrypt($user->password);
                } catch (DecryptException $decryptException) {
                    $decPassword = null;
                }

                if($decPassword == $password) {
                    $user->token = Token::generate($user, $expiredAt);
                    $user->expired_at = $expiredAt;

                    return Response::Success($user);
                }
            }

            return Response::Error(AuthError::class, 'LOGIN_FAILED',
                Response::HTTP_BAD_REQUEST);
        }

        return Response::Error(AuthError::class, 'LOGIN_FAILED',
            Response::HTTP_BAD_REQUEST);
    }

    public function create() {
        $rules = [
            'location_id'       => 'required|exists:locations,id',
            'full_name'         => 'required',
            'username'          => 'required|unique:users,username',
            'email'             => 'required|unique:users,email',
            'address'           => 'required|min:7',
            'postal_code'       => 'required|max:5',
            'phone_number'      => 'required|min:9',
            'password'          => 'required|min:8',
            'is_admin'          => 'required|boolean'
        ];

        return $this->validateRequest($rules, function ($validRequest) {
            $validRequest['password'] = Crypt::encrypt($validRequest['password']);

            /** @var User $user */
            $user = User::query()->create($validRequest);

            if(isset($user->id)) return $this->encSuccess($user);
            return Response::Error(UserError::class, 'FAIL_INSERT',
                Response::HTTP_INTERNAL_SERVER_ERROR);
        });
    }

    public function update($userID) {
        $rules = [
            'id'                => 'required:exists:user,id',
            'location_id'       => 'exists:locations,id',
            'full_name'         => '',
            'username'          => 'unique:users,username',
            'email'             => 'unique:users,email|email',
            'address'           => 'min:7',
            'postal_code'       => 'max:5',
            'phone_number'      => 'min:9',
            'is_admin'          => 'boolean'
        ];

        $this->request->replace(array_merge($this->request->all(), ['id' => $userID]));

        return $this->validateRequest($rules, function ($validRequest) {
            $user = User::query()->find($validRequest['id']);
            $user->update($validRequest);
            return $this->encSuccess($user);
        });
    }

    public function changePassword() {

    }
}
