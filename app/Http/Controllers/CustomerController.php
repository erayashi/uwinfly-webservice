<?php
/**
 * Created by eaz.
 * Date: 16/03/19
 * Time: 21.46
 * Github: https://github.com/erry-az
 */

namespace App\Http\Controllers;


use App\Customs\Response;
use App\Models\Customer;

class CustomerController extends AController
{
    public function gets() {
        $customerBuilder = Customer::query();
        return $this->encPaging($customerBuilder, $this->request);
    }

    public function get($customerID) {
        $customer = Customer::query()->find($customerID);
        if($customer) return $this->encSuccess($customer);
        return Response::notfound();
    }

    public function add() {
        $rules = [
            'name'          => 'required',
            'address'       => 'required',
            'email'         => 'required|unique:customers,email|email',
            'phone_number'  => 'required|unique:customers,phone_number'
        ];

        return $this->validateRequest($rules, function ($validRequest) {
            $newCustomer = Customer::query()->create($validRequest);
            return $this->encSuccess($newCustomer);
        });
    }
}
