<?php

namespace App\Http\Controllers;

use App\Customs\AES128;
use App\Customs\Response;
use App\Errors\ValidateError;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller as BaseController;

class AController extends BaseController
{
    /** @var Request $request */
    protected $request;
    /** @var string|null $token */
    protected $token;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->token = $request->header('token');
    }

    public function validateRequest($rules, callable $handle, $message = [], $customAttributes = []) {
        try {
            $validate = $this->validate($this->request, $rules, $message, $customAttributes);
            return $handle($validate);
        } catch (ValidationException $e) {
            return Response::Error(ValidateError::class, 'VALIDATE_FAIL',
                Response::HTTP_BAD_REQUEST, $e->errors());
        }
    }

    public function getBasic($explode = false) {
        $auth = $this->request->header('Authorization');
        if(strpos($auth, 'Basic') !== false) {
            $auth = str_replace('Basic ', '', $auth);
            $authDecode = base64_decode($auth);
            if($authDecode === false) return null;
            if($explode) return explode(':', $authDecode);
            return $authDecode;
        }

        return null;
    }

    protected function encSuccess($data, string $message = 'SUCCESS') {
        $success = Response::Success($data, $message);
        return AES128::encrypt($success, $this->token);
    }

    protected function encPaging(Builder $queryBuilder, Request $request) {
        $paging = Response::Paging($queryBuilder, $request);
        return AES128::encrypt($paging, $this->token);
    }

    /**
     * @return User
     */
    protected function getToken() {
        return $this->request->token;
    }
}
