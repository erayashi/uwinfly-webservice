<?php
/**
 * Created by eaz.
 * Date: 22/03/19
 * Time: 18.22
 * Github: https://github.com/erry-az
 */

namespace App\Http\Controllers;


use App\Customs\Response;
use App\Errors\ValidateError;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\Sale;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class OrderController extends AController
{
    public function gets() {
        $orderQuery = Order::query()->with('products');
        return $this->encPaging($orderQuery, $this->request);
    }

    public function get($orderID) {
        $order = Order::query()->with('products')->find($orderID);
        if($order) $this->encSuccess($order);

        return Response::notfound();
    }

    public function create() {
        $rules = [
            'sales_id'                  => 'required|exists:sales,id',
            'customer_id'               => 'required|exists:customers,id',
            'remark'                    => ['required', Rule::in(Order::REMARK_LIST)],
            'products'                  => 'required|array',
            'products.*.product_id'     => 'required|exists:products,id',
            'products.*.quantity'       => 'required|integer|gt:0',
        ];

        return $this->validateRequest($rules, function ($validRequest) {
            $products = $validRequest['products'];
            unset($validRequest['products']);
            $validRequest['user_id'] = $this->getToken()->id;

            try {
                $orderID = DB::transaction(function () use ($validRequest, $products) {
                    /** @var Sale $getSales */
                    $getSales = Sale::query()->find($validRequest['sales_id']);
                    /** @var Customer $getCustomer */
                    $getCustomer = Customer::query()->find($validRequest['customer_id']);

                    $validRequest['sales_name'] = $getSales->name;
                    $validRequest['customer_name'] = $getCustomer->name;
                    /** @var Order $order */
                    $order = Order::query()->create($validRequest);

                    foreach ($products as $i => $product) {
                        $productID = $product['product_id'];
                        $quantity = $product['quantity'];

                        /** @var Product $getProduct */
                        $getProduct = Product::query()->find($product['product_id']);

                        //validate stock
                        if ($getProduct->stock < 1)
                            throw new Exception("$getProduct->id - $getProduct->type $getProduct->color 
                        : Out of stock");

                        $products[$i]['price'] = $getProduct->price;
                        $products[$i]['order_id'] = $order->id;
                        $products[$i]['product_type'] = $getProduct->type;
                        $products[$i]['product_color'] = $getProduct->color;

                        Product::query()->where('id', $productID)
                            ->update(['stock' => DB::raw("stock - $quantity")]);
                    }

                    OrderProduct::query()->insert($products);

                    return $order->id;
                }, 5);
            } catch (Exception $e) {
                return Response::Error(ValidateError::class, 'VALIDATE_FAIL',
                    Response::HTTP_BAD_REQUEST, $e->getMessage());
            }

            return $this->encSuccess(Order::with('products')->find($orderID));
        });
    }

    public function delete($orderID) {
        $order = Order::query()->where('user_id', $this->getToken()->id)->where('id', $orderID)
            ->first();
        if(is_null($order)) return Response::notfound();

        $deletedOrder = clone $order;
        try {
            $order->delete();
        } catch (Exception $e) {
            Response::internalError();
        }

        return $this->encSuccess($deletedOrder, 'DELETE SUCCESS');
    }
}
