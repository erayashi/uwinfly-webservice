<?php
/**
 * Created by eaz.
 * Date: 05/04/19
 * Time: 14.08
 * Github: https://github.com/erry-az
 */

namespace App\Http\Controllers;


use App\Models\Location;
use Illuminate\Support\Carbon;

class LocationController extends AController
{
    public function gets() {
        $locationBuilder = Location::query();
        return $this->encPaging($locationBuilder, $this->request);
    }

    public function getReport() {
        $yearMonth = $this->request->query('year_month');
        if(!$yearMonth) $yearMonth = Carbon::now()->format('Y-m');
        return $this->encSuccess(Location::getReports($yearMonth));
    }

    public function create() {
        $rules = [
            'location' => 'required|unique:locations,location'
        ];

        return $this->validateRequest($rules, function ($validRequest) {
            $location = Location::query()->create($validRequest);
            return $this->encSuccess($location);
        });
    }
}
