<?php
/**
 * Created by eaz.
 * Date: 16/03/19
 * Time: 23.07
 * Github: https://github.com/erry-az
 */

namespace App\Http\Controllers;


use App\Customs\Response;
use App\Models\Product;

class ProductController extends AController
{
    public function gets() {
        $productBuilder = Product::query();
        return $this->encPaging($productBuilder, $this->request);
    }

    public function get($productID) {
        $product = Product::query()->find($productID);
        if($product) return $this->encSuccess($product);
        return Response::notfound();
    }

    public function create() {
        $rules = [
            'type'  => 'required',
            'color' => 'required',
            'price' => 'required|numeric|min:1',
            'stock' => 'required|integer|min:1'
        ];

        return $this->validateRequest($rules, function ($validRequest) {
            $newProduct = Product::query()->create($validRequest);
            return $this->encSuccess($newProduct);
        });
    }

    public function update($productID) {
        $rules = [
            'type'      => '',
            'color'     => '',
            'price'     => 'numeric|gt:0',
            'stock'     => 'integer|gt:0'
        ];

        $product = Product::query()->find($productID);
        if(is_null($product)) return Response::notfound();

        return $this->validateRequest($rules, function ($validRequest) use ($product) {
            $product->update($validRequest);
            return $this->encSuccess($product);
        });
    }

    public function delete($productID) {
        $product = Product::query()->find($productID);
        if(is_null($product)) return Response::notfound();

        $deletedProduct = clone $product;
        try {
            $product->delete();
        } catch (\Exception $e) {
            Response::internalError();
        }

        return $this->encSuccess($deletedProduct, 'DELETE SUCCESS');
    }

    public function updateStock($productID) {
        $rules = [
            'stock' => 'required|integer|gt:0'
        ];

        $product = Product::query()->find($productID);
        if(is_null($product)) return Response::notfound();

        return $this->validateRequest($rules, function ($validRequest) use ($product) {
            $product->update($validRequest);

            return $this->encSuccess($product);
        });
    }
}
