<?php
/**
 * Created by eaz.
 * Date: 20/03/19
 * Time: 18.07
 * Github: https://github.com/erry-az
 */

namespace App\Http\Controllers;


use App\Customs\Response;
use App\Models\Sale;
use Exception;

class SalesController extends AController
{
    public function gets() {
        $sales = Sale::query()->selectRaw('id,name,address,phone_number,created_at,updated_at')
            ->where('is_active', 1);
        return $this->encPaging($sales, $this->request);
    }

    public function get($salesID) {
        $sales = Sale::query()->where('id', $salesID)->where('is_active', 1)->first();
        if($sales) return $this->encSuccess($sales);
        return Response::notfound();
    }

    public function create() {
        $rules = [
            'name'          => 'required',
            'address'       => 'required|min:7',
            'phone_number'  => 'required|min:8'
        ];

        return $this->validateRequest($rules, function ($validRequest) {
            $sales = Sale::query()->create($validRequest);
            return $this->encSuccess($sales);
        });
    }

    public function update($salesID) {
        $rules = [
            'name'          => '',
            'address'       => 'min:7',
            'phone_number'  => 'min:8',
            'is_active'     => 'boolean'
        ];

        $sales = Sale::query()->find($salesID);
        if(is_null($sales)) return Response::notfound();

        return $this->validateRequest($rules, function ($validRequest) use ($sales) {
            $sales->update($validRequest);
            return $this->encSuccess($sales);
        });
    }

    public function delete($salesID) {
        $sales = Sale::query()->find($salesID);
        if(is_null($sales)) return Response::notfound();

        $salesDelete = clone $sales;
        try {
            $sales->delete();
        } catch (Exception $e) {
            return Response::internalError();
        }

        return $this->encSuccess($salesDelete, 'Delete sales success');
    }

    public function disable($salesID) {
        /** @var Sale $sales */
        $sales = Sale::query()->find($salesID);
        if(is_null($sales)) return Response::notfound();
        $sales->is_active = 0;
        $sales->save();

        return $this->encSuccess($sales, 'Disable sales success');
    }
}
