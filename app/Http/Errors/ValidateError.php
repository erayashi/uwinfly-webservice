<?php
/**
 * Created by eaz.
 * Date: 06/01/19
 * Time: 16:08
 * Github: https://github.com/erry-az
 */

namespace App\Errors;


class ValidateError
{
    const VALIDATE_FAIL = "Failed while validating request.";
    const NOT_FOUND = "Access not found.";
}
