<?php
/**
 * Created by eaz.
 * Date: 04/03/19
 * Time: 11.57
 * Github: https://github.com/erry-az
 */

namespace App\Http\Errors;


class AuthError
{
    const HEADERS_NOT_COMPLETE = 'token is required';
    const CHECK_FAILED = 'token is invalid';
    const LOGIN_FAILED = "Username/email with password didn't match";
    const ACCESS_INVALID = 'You don\'t have access to visit this page';
}
