<?php
/**
 * Created by eaz.
 * Date: 20/03/19
 * Time: 16.09
 * Github: https://github.com/erry-az
 */

namespace App\Http\Errors;


class UserError
{
    const FAIL_INSERT = 'Failed when create user';
}
