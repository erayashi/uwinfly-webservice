<?php
/**
 * Created by eaz.
 * Date: 10/03/19
 * Time: 11.33
 * Github: https://github.com/erry-az
 */

namespace App\Models;

/**
 * Class Product
 * @package App\Models
 *
 * @property int $stock
 * @property double $price
 * @property string $type
 * @property string $color
 */
class Product extends BaseModel
{
    public $filter = ['type', 'color', 'price', 'stock'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->order_by = array_merge($this->order_by, ['type', 'color', 'price', 'stock']);
    }
}
