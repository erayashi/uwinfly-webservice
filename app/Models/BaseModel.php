<?php
/**
 * Created by eaz.
 * Date: 30/12/18
 * Time: 21:34
 * Github: https://github.com/erry-az
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseModel
 * @package App\Models
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 */
class BaseModel extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public $default_order = self::UPDATED_AT;
    public $default_order_type = 'DESC';
    public $order_by = ['id', self::UPDATED_AT, self::CREATED_AT];
    public $order_type = ['ASC', 'DESC'];
    public $max_limit = 100;
    public $filter = ['id'];

    public function getOrderType($orderType) {
        if(!empty($orderType)) {
            $orderType = strtoupper($orderType);
            if (in_array($orderType, $this->order_type)) return $orderType;
        }

        return $this->default_order_type;
    }

    public function getOrderBy($orderBy) {
        if(!empty($orderBy)) {
            $orderBy = strtolower($orderBy);
            $orderBys = array_map('strtolower', $this->order_by);
            if (in_array($orderBy, $orderBys)) return $orderBy;
        }

        return $this->default_order;
    }

    public function searchBy(Builder &$builder, $query) {
        if($query) {
            foreach ($this->filter as $i => $field) {
                if($i == 0) $builder->where($field, 'like', "%$query%");
                else $builder->orWhere($field, 'like', "%$query%");
            }
        }
    }
}
