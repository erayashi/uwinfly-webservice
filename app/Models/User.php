<?php
/**
 * Created by eaz.
 * Date: 10/03/19
 * Time: 11.34
 * Github: https://github.com/erry-az
 */

namespace App\Models;


/**
 * Class User
 * @package App\Models
 *
 * @property int $location_id
 * @property string $full_name
 * @property string $username
 * @property string $email
 * @property string $address
 * @property string $postal_code
 * @property string $phone_number
 * @property string $password
 * @property int $is_admin
 * @property string $last_login_at
 * @property string $token
 * @property string $expired_at
 */
class User extends BaseModel
{
    protected $hidden = ['password'];
}
