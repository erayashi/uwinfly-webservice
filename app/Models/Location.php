<?php
/**
 * Created by eaz.
 * Date: 10/03/19
 * Time: 11.33
 * Github: https://github.com/erry-az
 */

namespace App\Models;


class Location extends BaseModel
{
    public static function getReports($date) {
        $initReports = static::query()->selectRaw('id, location, 0 order_count, 0 sum_sales_product, 
        0 sum_amount_product')->orderBy('id')->get()->keyBy('id')->toArray();

        $reports = static::query()->selectRaw('locations.id, locations.location, COUNT(o.id) order_count, 
        IFNULL(SUM(op.quantity), 0) sum_sales_product, IFNULL(SUM(op.price), 0) sum_amount_product')
            ->join('users as u', 'locations.id', '=', 'u.location_id')
            ->join('orders as o', 'u.id', '=', 'o.user_id')
            ->join('order_products as op', 'o.id', '=', 'op.order_id')
            ->whereRaw("DATE_FORMAT(o.created_at, '%Y-%m') = ?", [$date])
            ->groupBy('locations.id', 'locations.location')
            ->orderBy('locations.id')->get()->keyBy('id');

        $newReports = [];
        foreach ($initReports as $key => $initReport) {
            $report = $reports->get($key);
            if($report) $newReports[] = $report->toArray();
            else $newReports[] = $initReport;
        }

        return $newReports;
    }
}
