<?php
/**
 * Created by eaz.
 * Date: 10/03/19
 * Time: 20.22
 * Github: https://github.com/erry-az
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;

class Token
{
    public static function generate($user, $expiredAt) {
        $data = [
            'id' => $user->id,
            'exp' => $expiredAt
        ];
        return Crypt::encrypt(json_encode($data));
    }

    /**
     * @param $token
     *
     * @return null|object
     * @throws DecryptException
     */
    public static function check($token) {
        $decryptedToken = Crypt::decrypt($token);
        //if decrypt token success
        if(!empty($decryptedToken)) {
            //json decode decrypted token
            $deserializeToken = json_decode($decryptedToken);
            //check token expired
            if(isset($deserializeToken->exp) && isset($deserializeToken->id)) {
                if($deserializeToken->exp < Carbon::now()->timestamp) throw new DecryptException('token is expired');
                else return $deserializeToken;
            } else throw new DecryptException('failed to decrypt token');
        }
        return null;
    }
}
