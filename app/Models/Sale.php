<?php
/**
 * Created by eaz.
 * Date: 10/03/19
 * Time: 11.34
 * Github: https://github.com/erry-az
 */

namespace App\Models;

/**
 * Class Sale
 * @package App\Models
 *
 * @property int $is_active
 * @property string $name
 * @property string $address
 * @property string $phone_number
 */
class Sale extends BaseModel
{
    protected $table = 'sales';
    public $filter = ['id', 'name', 'address', 'phone_number'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->order_by = array_merge($this->order_by, $this->filter);
    }
}
