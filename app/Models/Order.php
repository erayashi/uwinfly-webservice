<?php
/**
 * Created by eaz.
 * Date: 10/03/19
 * Time: 11.13
 * Github: https://github.com/erry-az
 */

namespace App\Models;


class Order extends BaseModel
{
    const REMARK_CASH = 'CASH';
    const REMARK_PAID = 'PAID';
    const REMARK_DEPOSIT = 'DEPOSIT';

    const REMARK_LIST = [self::REMARK_CASH, self::REMARK_PAID, self::REMARK_DEPOSIT];

    public $filter = ['sales_name', 'customer_name', 'id'];

    public function products() {
        return $this->hasMany(OrderProduct::class);
    }
}
