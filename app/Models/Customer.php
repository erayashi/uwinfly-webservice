<?php
/**
 * Created by eaz.
 * Date: 10/03/19
 * Time: 11.32
 * Github: https://github.com/erry-az
 */

namespace App\Models;

/**
 * Class Customer
 * @package App\Models
 *
 * @property string $name
 * @property string $address
 * @property string $phone_number
 * @property string $email
 */
class Customer extends BaseModel
{
    public $filter = ['id', 'name', 'address', 'email', 'phone_number'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->order_by = array_merge($this->order_by, ['name', 'email', 'phone_number']);
    }
}
