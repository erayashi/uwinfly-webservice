<?php


namespace App\Customs;


class AES128 {
    private static $IV = 'InitVector';

    public static function encrypt($clear_text, $key, $iv = null) {
        if(is_null($iv)) $iv = static::$IV;
        $iv = str_pad($iv, 16, "\0");
        $encrypt_text = openssl_encrypt($clear_text, "AES-128-CBC", $key, OPENSSL_RAW_DATA, $iv);
        $data = base64_encode($encrypt_text);
        return $data;
    }

    public static function decrypt($data, $key, $iv = null) {
        if(is_null($iv)) $iv = static::$IV;
        $iv = str_pad($iv, 16, "\0");
        $encrypt_text = base64_decode($data);
        $clear_text = openssl_decrypt($encrypt_text, "AES-128-CBC", $key, OPENSSL_RAW_DATA, $iv);
        return $clear_text;
    }
}
