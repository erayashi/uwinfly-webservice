<?php
/**
 * Created by eaz.
 * Date: 05/01/19
 * Time: 7:43
 * Github: https://github.com/erry-az
 */

namespace App\Customs;


use Illuminate\Database\Connection;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @method static bool hasColumn($table, $column)
 * @method static bool hasColumns($table, array $column)
 * @method static array getColumnListing($table)
 * @method static enableForeignKeyConstraints()
 * @method static disableForeignKeyConstraints()
 * @method static Connection getConnection()
 */
class MySchema extends Schema
{
    protected static $table;

    private function makeUniqueKey($keys) {
        return strtolower(static::$table."_".implode("_", $keys)."_unique");
    }

    public static function init($table) {
        static::$table = $table;
        return new static;
    }

    public function getColumns() {
        return array_map('strtolower', static::getColumnListing(static::$table));
    }

    /**
     * @param $column string|array
     * @return bool
     */
    public function columnExists($column) {
        return is_array($column) ? static::hasColumns(static::$table, $column) :
            static::hasColumn(static::$table, $column);
    }

    /**
     * @param $column
     * @param \Closure $closure
     * @param bool $isExist
     */
    public function modifiedWhenColumn($column, \Closure $closure, $isExist = true) {
        if(static::columnExists($column) === $isExist){
            $this->modified($closure);
        }
    }

    public function modified(callable $closure){
        static::table(static::$table, $closure);
    }

    public function getKeys(){
        $sm = static::getConnection()->getDoctrineSchemaManager();
        return array_map('strtolower', collect($sm->listTableIndexes(static::$table))->keys()->toArray());
    }

    public function removeUniqueWhenExist(array $keys) {
        $this->modified(function (Blueprint $table) use ($keys){
            foreach ($this->keyExists($keys) as $key) {
                $table->dropUnique($key);
            }
        });
    }

    public function removeForeignWhenExist(array $keys) {
        $this->modified(function (Blueprint $table) use ($keys){
            foreach ($this->keyExists($keys) as $key) {
                $table->dropForeign($key);
            }
        });
    }

    public function keyExists(array $keys) {
        if(!empty($keys)) {
            $keyExists = $this->getKeys();
            if(is_array($keys[0])) {
                foreach ($keys as $subKey) {
                    $key = $this->makeUniqueKey($subKey);
                    if(in_array($key, $keyExists)) yield $key;
                }
            } else {
                $key = $this->makeUniqueKey($keys);
                if(in_array($key, $keyExists)) yield $key;
            }
        }
    }

    public function dropColumnIfExist($columns){
        $deletedColumns = [];
        $tableColumns = $this->getColumns();
        if(is_array($columns)) foreach ($columns as $column) {
            if(in_array($column, $tableColumns)) $deletedColumns[] = $column;
        }
        else $deletedColumns = in_array($columns, $tableColumns) ? $columns : null;

        if(!empty($deletedColumns)) {
            $this->modified(function (Blueprint $table) use ($deletedColumns) {
                $table->dropColumn($deletedColumns);
            });
        }
        unset($deletedColumns);
        unset($tableColumns);
    }
}
