<?php
/**
 * Created by eaz.
 * Date: 05/01/19
 * Time: 7:41
 * Github: https://github.com/erry-az
 */

namespace App\Customs;


use Illuminate\Database\Schema\Blueprint;

class MyBlueprint
{
    protected $bp;

    public function __construct(Blueprint $bp)
    {
        $this->bp = $bp;
    }
}